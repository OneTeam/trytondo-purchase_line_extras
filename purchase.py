# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.model import ModelView, ModelSQL, fields
from trytond.modules.currency.fields import Monetary

from trytond.exceptions import UserError

from decimal import Decimal

class Line(metaclass=PoolMeta):
    'Purchase Line Extras'
    __name__ = 'purchase.line'

    tax_amount = fields.Function(Monetary("Tax", currency='currency', digits='currency'),
                                 'get_amount_taxes')
    total_with_taxes = fields.Function(Monetary("Total With Taxes", currency='currency', digits='currency'),
                                       'get_total_with_taxes')

    
    @fields.depends('type', 'quantity', 'unit_price', 'unit', 'purchase',
                    '_parent_purchase.currency', 'taxes', 'amount')
    def on_change_with_tax_amount(self):
        Decimal('0.0')
        if self.type == 'line':
            tax_amount = Decimal('0.0')
            currency = self.purchase.currency if self.purchase else None
            for tax in self.taxes:
                tax_amount += Decimal(str((tax.rate * self.amount) or '0.0'))
            if currency:
                return currency.round(tax_amount)
            return tax_amount
        return Decimal('0.0')


    @fields.depends('type', 'quantity', 'unit_price', 'unit', 'purchase',
                    '_parent_purchase.currency', 'taxes', 'amount')
    def on_change_with_total_with_taxes(self):
        try:
            total_with_taxes = self.amount + self.tax_amount
            
            return total_with_taxes
        except:
            pass

    @fields.depends('product', 'unit', 'purchase',
        '_parent_purchase.party', '_parent_purchase.invoice_party',
        'product_supplier', methods=['compute_taxes', 'compute_unit_price',
                                     '_get_product_supplier_pattern'])
    def on_change_product(self):
        self.tax_amount = self.on_change_with_tax_amount()
        self.total_with_taxes = self.on_change_with_total_with_taxes()
        super(Line, self).on_change_product()
        
    def get_amount_taxes(self, name):
        if self.type == 'line':
            return self.on_change_with_tax_amount()

        
    def get_total_with_taxes(self, name):
        if self.type == 'line':
            return self.on_change_with_total_with_taxes()

    
