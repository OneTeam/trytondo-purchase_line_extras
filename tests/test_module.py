# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class PurchaseLineExtrasTestCase(ModuleTestCase):
    "Test Purchase Line Extras module"
    module = 'purchase_line_extras'


del ModuleTestCase
